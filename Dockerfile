# Use uma imagem base do Node.js para a aplicação
FROM node:14 AS app

# Defina o diretório de trabalho para a aplicação
WORKDIR /usr/src/app

# Copie os arquivos de aplicação para o contêiner
COPY package*.json ./
COPY app.js .

# Instale as dependências da aplicação
RUN npm install

# Exponha a porta 3000
EXPOSE 3000

# Comando para iniciar a aplicação
CMD [ "node", "app.js" ]


# Use a imagem do APM Server como base para o monitoramento
FROM docker.elastic.co/apm/apm-server:7.16.1 AS apm-server

# Copie o arquivo de configuração do APM Server
COPY apm-server.yml /usr/share/apm-server/apm-server.yml

# Exponha a porta 8200 para o APM Server
EXPOSE 8200

# Comando para iniciar o APM Server
CMD ["apm-server", "-e"]

