const express = require('express');

describe('app.js', () => {
    it('should require express module', () => {
        expect(express).toBeDefined();
    });
});