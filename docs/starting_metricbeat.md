
sudo chown root:root /home/nataliagranato/Documentos/JLCP/apm-server/metricbeat.docker.yml

sudo chmod go-w /home/nataliagranato/Documentos/JLCP/apm-server/metricbeat.docker.yml

docker run -d \
  --name=metricbeat \
  --user=root \
  --volume="/home/nataliagranato/Documentos/JLCP/apm-server/metricbeat.docker.yml:/usr/share/metricbeat/metricbeat.yml:ro" \
  --volume="/var/run/docker.sock:/var/run/docker.sock:ro" \
  --volume="/sys/fs/cgroup:/hostfs/sys/fs/cgroup:ro" \
  --volume="/proc:/hostfs/proc:ro" \
  --volume="/:/hostfs:ro" \
  docker.elastic.co/beats/metricbeat:7.16.3 metricbeat -e \
  -E output.elasticsearch.hosts=["http://elasticsearch:9200"]


### Comandos

1. `sudo chown root:root /home/nataliagranato/Documentos/JLCP/apm-server/metricbeat.docker.yml`: Este comando muda a propriedade do arquivo `metricbeat.docker.yml` para o usuário root e o grupo root. `sudo` é usado para executar o comando com privilégios de superusuário, `chown` é o comando para mudar a propriedade de um arquivo, e `root:root` especifica o novo proprietário e grupo (ambos root).

2. `sudo chmod go-w /home/nataliagranato/Documentos/JLCP/apm-server/metricbeat.docker.yml`: Este comando remove as permissões de escrita para o grupo e outros usuários no arquivo `metricbeat.docker.yml`. `chmod` é o comando para mudar as permissões de um arquivo, e `go-w` especifica que as permissões de escrita (w) devem ser removidas para o grupo (g) e outros usuários (o).

3. `docker run -d`: Este comando inicia um novo contêiner Docker em segundo plano (devido à opção `-d`, que significa "detached mode").

4. `--name=metricbeat`: Esta opção dá ao contêiner o nome "metricbeat".

5. `--user=root`: Esta opção faz com que o contêiner seja executado como o usuário root.

6. `--volume="/home/nataliagranato/Documentos/JLCP/apm-server/metricbeat.docker.yml:/usr/share/metricbeat/metricbeat.yml:ro"`: Esta opção monta o arquivo `metricbeat.docker.yml` do host no contêiner no caminho especificado. A opção `:ro` faz com que o volume seja montado como somente leitura.

7. `--volume="/var/run/docker.sock:/var/run/docker.sock:ro"`: Este comando monta o socket Docker do host no contêiner, permitindo que o contêiner interaja com o daemon Docker no host.

8. `--volume="/sys/fs/cgroup:/hostfs/sys/fs/cgroup:ro"`, `--volume="/proc:/hostfs/proc:ro"`, `--volume="/:/hostfs:ro"`: Estes comandos montam vários diretórios do sistema do host no contêiner, permitindo que o Metricbeat monitore o sistema do host.

9. `docker.elastic.co/beats/metricbeat:7.16.3`: Este é o nome da imagem Docker que será usada para criar o contêiner. Neste caso, é a imagem do Metricbeat versão 7.16.3 do repositório oficial da Elastic no Docker Hub.

10. `metricbeat -e`: Este é o comando que será executado quando o contêiner iniciar. `metricbeat` é o comando para iniciar o Metricbeat, e a opção `-e` faz com que o Metricbeat execute em modo de console, o que significa que ele irá logar na saída padrão em vez de usar um arquivo de log.

9. `docker.elastic.co/beats/metricbeat:7.16.3`: Este é o nome da imagem Docker que será usada para criar o contêiner. Neste caso, é a imagem do Metricbeat versão 7.16.3 do repositório oficial da Elastic no Docker Hub.

10. `metricbeat -e`: Este é o comando que será executado quando o contêiner iniciar. `metricbeat` é o comando para iniciar o Metricbeat, e a opção `-e` faz com que o Metricbeat execute em modo de console, o que significa que ele irá logar na saída padrão em vez de usar um arquivo de log.

11. `-E output.elasticsearch.hosts=["http://elasticsearch:9200"]`: Esta é uma opção de linha de comando para o Metricbeat que define a configuração `output.elasticsearch.hosts` para o valor especificado. Neste caso, está definindo o host Elasticsearch para "http://elasticsearch:9200". A opção `-E` permite que você substitua configurações individuais na linha de comando.

Em resumo, este comando inicia um contêiner Docker chamado "metricbeat" usando a imagem do Metricbeat versão 7.16.3. O contêiner é executado como o usuário root e tem vários volumes montados do host, incluindo o arquivo de configuração do Metricbeat e vários diretórios do sistema. Quando o contêiner inicia, ele executa o Metricbeat em modo de console com a configuração `output.elasticsearch.hosts` definida para "http://elasticsearch:9200".